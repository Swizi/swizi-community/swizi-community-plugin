var version = __VERSION__;
var isIOS = false;
var pluginIOSBridge;
var promises = {};

if (typeof plugins === "undefined") {
  if (
    window.webkit &&
    window.webkit.messageHandlers &&
    window.webkit.messageHandlers.plugins
  ) {
    pluginIOSBridge = window.webkit.messageHandlers.plugins;
    isIOS = true;
  }
}

var swizi = {};

swizi.isFullscreen = false;

swizi.addListenerMulti = (element, eventNames, listener) => {
  var events = eventNames.split(" ");
  for (var i = 0, iLen = events.length; i < iLen; i++) {
    element.addEventListener(events[i], listener, false);
  }
};

swizi.removeListenerMulti = (element, eventNames, listener) => {
  var events = eventNames.split(" ");
  for (var i = 0, iLen = events.length; i < iLen; i++) {
    element.removeEventListener(events[i], listener, false);
  }
};

swizi.generateUUID = () => {
  var d = new Date().getTime();
  var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (
    c
  ) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
  });
  return uuid;
};

swizi.resolvePromise = (promiseId, data, error) => {
  if (error) {
    promises[promiseId].reject(data);
  } else {
    promises[promiseId].resolve(data);
  }
  delete promises[promiseId];
};

swizi.call = (method, args) => {
  var promise = new Promise(function (resolve, reject) {
    var promiseId = swizi.generateUUID();
    promises[promiseId] = { resolve: resolve, reject: reject };

    try {
      pluginIOSBridge.postMessage({
        promiseId: promiseId,
        method: method,
        args: args || []
      });
    } catch (exception) {
      alert(exception);
    }
  });
  return promise;
};

swizi.isPluginBridgeReady = () => {
  if (typeof pluginIOSBridge != "undefined" || typeof plugins != "undefined")
    return true;
  else return false;
};

swizi.log = (type, msg) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "log", args: [type, msg] });
    } else {
      console[type](msg);
    }
  } else {
    return null;
  }
};

swizi.navigateTo = (filename, type, animated) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "showPageWithPresentationTypeAnimated",
        args: [filename, type, animated]
      });
    } else {
      plugins.showPageWithPresentationTypeAnimated(filename, type, animated);
    }
  } else {
    return null;
  }
};

swizi.back = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "goBackToPreviousPage" });
    } else {
      plugins.goBackToPreviousPage();
    }
  } else {
    return null;
  }
};

swizi.getPlatform = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getPlatform", null).then(function (datas) {
        return datas;
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getPlatform());
      });
      q = q.then(function (datas) {
        return datas;
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getPlatformVersion = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getPlatformVersion", null).then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getPlatformVersion());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getUser = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getUser", null).then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getUser());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getToken = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getToken", null).then(function (datas) {
        return datas;
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getToken());
      });
      q = q.then(function (datas) {
        return datas;
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getAuthWSToken = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getAuthWSToken", null).then(function (datas) {
        return datas;
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getAuthWSToken());
      });
      q = q.then(function (datas) {
        return datas;
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getUserPhoto = userID => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      var args = [];
      if (userID) {
        args.push(userID);
      }
      pluginIOSBridge.postMessage({ method: "getUserPhoto", args: args });
    } else {
      plugins.getUserPhoto(userID);
    }
  } else {
    return null;
  }
};

swizi.getUserLocation = (oneShot = false, apiKey, preGPS = false) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      var args = [];
      args.push(oneShot);
      if (apiKey) {
        args.push(apiKey);
        args.push(preGPS);
      }
      pluginIOSBridge.postMessage({ method: "getUserLocation", args: args });
    } else {
      plugins.getUserLocation(apiKey, oneShot, preGPS);
    }
  } else {
    return null;
  }
};

swizi.getColor = colorName => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi
        .call("getApplicationColorWithID", [colorName])
        .then(function (datas) {
          if (datas) {
            return JSON.parse(datas);
          } else {
            return null;
          }
        });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getApplicationColorWithID(colorName));
      });
      q = q.then(function (datas) {
        if (datas) {
          return JSON.parse(datas);
        } else {
          return null;
        }
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.setFullscreen = withAnimation => {
  if (swizi.isPluginBridgeReady()) {
    swizi.isFullscreen = !swizi.isFullscreen;
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "useFullScreen",
        args: [swizi.isFullscreen, withAnimation]
      });
    } else {
      plugins.useFullScreen(swizi.isFullscreen);
    }
  } else {
    return null;
  }
};

swizi.setTitle = title => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "setViewTitle", args: [title] });
    } else {
      plugins.setViewTitle(title);
    }
  } else {
    return null;
  }
};

swizi.setStatusBarLight = isLight => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "setStatusBarLight",
        args: [isLight]
      });
    } else {
      plugins.setStatusBarLight(isLight);
    }
  } else {
    return null;
  }
};

swizi.setStatusColor = color => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "setStatusColor", args: [color] });
    } else {
      plugins.setStatusColor(color);
    }
  } else {
    return null;
  }
};

swizi.readQRCode = (title = "", fullscreen = false, multipleTimes = false) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "readQRCode", args: [title, fullscreen, multipleTimes] });
    } else {
      plugins.readQRCode(title);
    }
  } else {
    return null;
  }
};

swizi.takePicture = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "takePicture" });
    } else {
      plugins.takePicture();
    }
  } else {
    return null;
  }
};

swizi.getGenericActions = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getGenericActions", null).then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getGenericActions());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.performGenericAction = (actionID, params) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "performGenericAction",
        args: [actionID, params]
      });
    } else {
      plugins.performGenericAction(actionID, params);
    }
  } else {
    return null;
  }
};

swizi.getItems = label => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi
        .call("getAPIKeyValuesWithLabel", [label])
        .then(function (datas) {
          return JSON.parse(datas);
        });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getAPIKeyValuesWithLabel(label));
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getItemForKey = (key, label) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi
        .call("getAPIKeyValueFromKeyWithLabel", [key, label])
        .then(function (datas) {
          return datas;
        });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getAPIKeyValueFromKeyWithLabel(key, label));
      });
      q = q.then(function (datas) {
        return datas;
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.setItemForKey = (item, key, label) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "setAPIKeyValueWithLabel",
        args: [key, item, label]
      });
    } else {
      plugins.setAPIKeyValueWithLabel(key, item, label);
    }
  } else {
    return null;
  }
};

swizi.removeItemForKey = key => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "deleteAPIKey", args: [key] });
    } else {
      plugins.deleteAPIKey(key);
    }
  } else {
    return null;
  }
};

swizi.getFiles = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getFiles", null).then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getFiles());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getFile = URL => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getFile", [URL]).then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getFile(URL));
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getFileNamed = filename => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getFileNamed", [filename]).then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getFileNamed(filename));
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.saveFileNamed = (base64String, filename) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi
        .call("saveFileNamed", [base64String, filename])
        .then(function (datas) {
          return JSON.parse(datas);
        });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.saveFileNamed(base64String, filename));
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.activateGPSTracking = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "activateGPSTracking" });
    } else {
      plugins.activateGPSTracking();
    }
  } else {
    return null;
  }
};

swizi.deactivateGPSTracking = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "deactivateGPSTracking" });
    } else {
      plugins.deactivateGPSTracking();
    }
  } else {
    return null;
  }
};

swizi.isGPSTrackingEnabled = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("isGPSTrackingEnabled", null).then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.isGPSTrackingEnabled());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getForm = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getForm").then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getForm());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.showSpinner = (show, message) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "showSpinner",
        args: [show, message ? message : null]
      });
    } else {
      plugins.showSpinner(show, message ? message : null);
    }
  } else {
    return null;
  }
};

swizi.getNavBarSize = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getNavBarSize").then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getNavBarSize());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getStatusBarSize = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getStatusBarSize").then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getStatusBarSize());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.openMenu = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "openMenu",
        args: []
      });
    } else {
      plugins.openMenu();
    }
  } else {
    return null;
  }
};

swizi.getLang = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getLang").then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getLang());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getSpaceId = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getSpaceId").then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.getSpaceId());
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.changeSpaceId = spaceId => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("changeSpaceId", [spaceId]).then(function (datas) {
        return JSON.parse(datas);
      });
    } else {
      var q = new Promise(function (resolve, reject) {
        resolve(plugins.changeSpaceId(spaceId));
      });
      q = q.then(function (datas) {
        return JSON.parse(datas);
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.setNavbarButton = (idButton, pathToIcon) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "setNavbarButton",
        args: [idButton, pathToIcon]
      });
    } else {
      plugins.setNavbarButton(idButton, pathToIcon);
    }
  } else {
    return null;
  }
};

swizi.removeNavbarButton = idButton => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "removeNavbarButton",
        args: [idButton]
      });
    } else {
      plugins.removeNavbarButton(idButton);
    }
  } else {
    return null;
  }
};

swizi.closeApp = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      // Not implemented
    } else {
      if (plugins.closeApp)
        plugins.closeApp();
    }
  } else {
    return null;
  }
};

swizi.setPluginControlsBackPress = takesControl => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "setPluginControlsBackPress",
        args: [takesControl]
      });
    } else {
      plugins.setPluginControlBackPress(takesControl);
    }
  } else {
    return null;
  }
};

swizi.setNavbarChildMode = isChild => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "setNavbarChildMode",
        args: [isChild]
      });
    } else {
      plugins.setNavbarChildMode(isChild);
    }
  } else {
    return null;
  }
};

swizi.refreshUserDatas = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "refreshUserDatas",
        args: []
      });
    } else {
      plugins.refreshUserDatas();
    }
  } else {
    return null;
  }
};

swizi.goToWelcome = popToRoot => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "goToWelcome",
        args: [popToRoot]
      });
    } else {
      plugins.goToWelcome(popToRoot);
    }
  } else {
    return null;
  }
};

swizi.goToNotificationCenter = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({ method: "showNotificationCenter" });
    } else {
      plugins.showNotificationCenter();
    }
  } else {
    return null;
  }
};

swizi.getManifest = () => {
  if (swizi.isPluginBridgeReady()) {
    const getFormatedConfig = configuration => {
      let formatedConfig = {};
      configuration.forEach(config => {
        formatedConfig = {
          ...formatedConfig,
          [config.name]:
            config.type === "BOOLEAN"
              ? config.value === "true"
              : config.type === "FLOAT"
                ? parseFloat(config.value)
                : config.type === "INT"
                  ? parseInt(config.value)
                  : config.type === "RANGE"
                    ? JSON.parse(config.range)
                    : config.type === "ENUM"
                      ? {
                        value: config.value,
                        enumValues: config.enumValues
                      }
                      : config.value
        };
      });
      return formatedConfig;
    };

    if (isIOS) {
      return swizi.call("getManifest", null).then(data => {
        data = JSON.parse(data);
        let formatedConfig = data.configuration
          ? getFormatedConfig(data.configuration)
          : {};
        return { ...data, configuration: formatedConfig };
      });
    } else {
      var promise = new Promise(function (resolve, reject) {
        var promiseId = swizi.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };
        plugins.getPluginManifest(promiseId);
      });

      return promise.then(data => {
        let formatedConfig = data.configuration
          ? getFormatedConfig(data.configuration)
          : {};
        return Promise.resolve({ ...data, configuration: formatedConfig });
      });
    }
  } else {
    return null;
  }
};

swizi.loginUser = (type = "LOGIN", login, password, activationCode) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("login", [type, login, password, activationCode]).then(function (data) {
        return JSON.parse(data);
      });
    } else {
      var promise = new Promise(function (resolve, reject) {
        var promiseId = swizi.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };
        plugins.loginUser(type, login, password, activationCode, promiseId);
      });

      return promise.then(data => {
        return JSON.parse(data);
      });
    }
  } else {
    return null;
  }
};

swizi.logoutUser = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("logout", []).then(function (data) {
        return JSON.parse(data);
      });
    } else {
      var promise = new Promise(function (resolve, reject) {
        var promiseId = swizi.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };
        plugins.logoutUser(promiseId);
      });

      return promise.then(data => {
        return JSON.parse(data);
      });
    }
  } else {
    return null;
  }
};

swizi.getMedia = (mediaId) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getMedia", [mediaId]).then(function (data) {
        return JSON.parse(data);
      });
    } else {
      var promise = new Promise(function (resolve, reject) {
        var promiseId = swizi.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };
        plugins.getMedia(mediaId, promiseId);
      });

      return promise.then(data => {
        return JSON.parse(data);
      });
    }
  } else {
    return null;
  }
};

swizi.openApp = (appUrl, inApp) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("openApp", [appUrl, inApp]).then(function (data) {
        return JSON.parse(data);
      });
    } else {
      var promise = new Promise(function (resolve, reject) {
        var promiseId = swizi.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };
        plugins.openApp(appUrl, inApp, promiseId);
      });

      return promise.then(data => {
        return JSON.parse(data);
      });
    }
  } else {
    return null;
  }
};

swizi.sendStat = (eventName, data) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("sendStat", [eventName, data]).then(function (data) {
        return JSON.parse(data);
      });
    } else {
      var promise = new Promise(function (resolve, reject) {
        var promiseId = swizi.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };
        plugins.sendStat(eventName, JSON.stringify(data), promiseId);
      });

      return promise.then(data => {
        return JSON.parse(data);
      });
    }
  } else {
    return null;
  }
};

swizi.toggleFab = hide => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      pluginIOSBridge.postMessage({
        method: "toggleFab",
        args: [hide]
      });
    } else {
      plugins.toggleFab(hide);
    }
  } else {
    return null;
  }
};

/** ------------ WIFI FEATURES ------------ */
swizi.getWifiInfos = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getWifiInfos", null).then(function(datas) {
        if (datas) {
          return JSON.parse(datas);
        } else {
          return null;
        }
      });
    } else {
      var q = new Promise(function(resolve, reject) {
        resolve(plugins.getWifiInfos());
      });
      q = q.then(function(datas) {
        if (datas) {
          return JSON.parse(datas);
        } else {
          return null;
        }
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.getWifiNetworkAvailable = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi.call("getWifiNetworkAvailable", null).then(function(datas) {
        return datas;
      });
    } else {
      plugins.getWifiNetworkAvailable();
    }
  } else {
    return null;
  }
};

swizi.connectToWifi = (ssid, pwd, wifiSecurity, identity, eapType, authType, captivePortal) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      return swizi
        .call("connectToWifi", [ssid, pwd, wifiSecurity, identity, eapType, authType])
        .then(function(datas) {
          if (datas) {
            return JSON.parse(datas);
          } else {
            return null;
          }
        });
    } else {
      var promise = new Promise(function (resolve, reject) {
        var promiseId = swizi.generateUUID();
        promises[promiseId] = { resolve: resolve, reject: reject };
        plugins.connectToWifi(promiseId, ssid, pwd, wifiSecurity, identity, eapType, authType, captivePortal)
      });

      return promise.then(data => {
        return JSON.parse(data);
      });
    }
  } else {
    return null;
  }
};

swizi.isWifiEnabled = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
     
    } else {
      var q = new Promise(function(resolve, reject) {
        resolve(plugins.isWifiEnabled());
      });
      q = q.then(function(datas) {
        if (datas) {
          return JSON.parse(datas);
        } else {
          return null;
        }
      });
      return q;
    }
  } else {
    return null;
  }
};

swizi.enableWifi = (enable) => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
    } else {
      plugins.enableWifi(enable)
    }
  } else {
    return null;
  }
};

swizi.getIp = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {} else {
      var q = new Promise(function(resolve, reject) {
        resolve(plugins.getIp());
      });
      q = q.then(function(datas) {
        if (datas) {
          return datas;
        } else {
          return null;
        }
      });
      return q;
    }
  } else {
    return null;
  }
};

/** ------------ END WIFI FEATURES ------------ */



/** ------------ NFC FEATURES ------------ */

swizi.enableNFC = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      
    } else {
      plugins.enableNFC();
    }
  } else {
    return null;
  }
};

swizi.disableNFC = () => {
  if (swizi.isPluginBridgeReady()) {
    if (isIOS) {
      
    } else {
      plugins.disableNFC();
    }
  } else {
    return null;
  }
};

/** ------------ END NFC FEATURES ------------ */



swizi.experimental = {
  ios: {
    dataConnector: (dataConnector, datas, withCompletion) => {
      if (swizi.isPluginBridgeReady() && isIOS) {
        swizi.log("experimental", [{ dataConnector, datas, withCompletion }]);
        return swizi
          .call("dataConnector", [{ dataConnector, datas, withCompletion }])
          .then(function (datas) {
            return JSON.parse(datas);
          });
      }
    },
    realFullScreen: () => {
      if (swizi.isPluginBridgeReady() && isIOS) {
        swizi.call("realFullScreen", [])
      }
    }
  }
};

swizi.onEvent = cb => {
  swizi.addListenerMulti(document, "swiziEvent", cb);
};

swizi.removeEvent = cb => {
  swizi.removeListenerMulti(document, "swiziEvent", cb);
};

document.documentElement.dataset.swizi = true;
document.documentElement.dataset.swiziVersion = version;

window.swizi = swizi;

module.exports = swizi;
