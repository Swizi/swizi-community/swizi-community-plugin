/* Swizi calls */
document.addEventListener("swiziReady", function() {
  //Wait for swiziReady event before starting anything
  document.addEventListener("swiziEvent", function(event) {
    console.log(event);
  });

  document.querySelector("#user").addEventListener("click", function() {
    if (swizi) {
      var that = this;
      swizi.getUser().then(function(datas) {
        that.innerText = JSON.stringify(datas);
      });
    }
  });

  document.querySelector("#token").addEventListener("click", function() {
    if (swizi) {
      var that = this;
      swizi.getToken().then(function(datas) {
        that.innerText = datas;
      });
    }
  });

  document.querySelector("#qrcode").addEventListener("click", function() {
    if (swizi) {
      swizi.readQRCode();
    }
  });

  document.querySelector("#color").addEventListener("click", function() {
    if (swizi) {
      var that = this;
      swizi.getColor("COLOR_ID_1").then(function(color) {
        if (color) {
          that.style.backgroundColor =
            "rgba(" +
            color.r +
            "," +
            color.g +
            "," +
            color.b +
            "," +
            color.a +
            ")";
        }
      });
    }
  });

  document.querySelector("#pushPage").addEventListener("click", function() {
    if (swizi) {
      swizi.navigateTo("index", null, true);
    }
  });

  document.querySelector("#presentPage").addEventListener("click", function() {
    if (swizi) {
      swizi.navigateTo("index", "modal", true);
    }
  });

  document.querySelector("#closePage").addEventListener("click", function() {
    if (swizi) {
      swizi.back();
    }
  });

  document
    .querySelector("#useFullScreen")
    .addEventListener("click", function() {
      if (swizi) {
        swizi.setFullscreen();
      }
    });

  document.querySelector("#setTitle").addEventListener("click", function() {
    if (swizi) {
      swizi.setTitle(randomTitle());
    }
  });

  document.querySelector("#setStatusBar").addEventListener("click", function() {
    if (swizi) {
      swizi.setStatusBarLight(true);
    }
  });

  document
    .querySelector("#setStatusBarDark")
    .addEventListener("click", function() {
      if (swizi) {
        swizi.setStatusBarLight(false);
      }
    });

  document
    .querySelector("#setShowLoading")
    .addEventListener("click", function() {
      if (swizi) {
        swizi.showSpinner(true, "Message");
        setTimeout(function() {
          swizi.showSpinner(false);
        }, 3000);
      }
    });
});

/* Util */
function randomTitle() {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
