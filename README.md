# Swizi Plugin JS

A Swizi app provides a way to access to native method such as retrieve the user, call generic actions or even getting a bar code inside your web plugin. 
To perform a native call, you need to import swizi.js or swizi.min.js in your index.html.
You must have swizi.js next to your index.html to be sure everything is called at the right moment.

# New in the 4.2.1 SDK

> [Close App (Android)](#close-app)

[Go to previous release notes](#previous-release-notes)

# Summary
- [Swizi Plugin JS](#swizi-plugin-js)
- [New in the 4.2.1 SDK](#new-in-the-421-sdk)
- [Summary](#summary)
- [Init](#init)
- [Export your plugin to use it in a Swizi app](#export-your-plugin-to-use-it-in-a-swizi-app)
- [Responses](#responses)
- [Logs](#logs)
	- [Check if plugin bridge is ready](#check-if-plugin-bridge-is-ready)
- [Native calls](#native-calls)
	- [Login user](#login-user)
	- [Logout user](#logout-user)
	- [Get connected user](#get-connected-user)
	- [Refresh user datas](#refresh-user-datas)
	- [Get current token](#get-current-token)
	- [Get AuthWS token](#get-authws-token)
	- [Get user profile photo](#get-user-profile-photo)
	- [Get user location](#get-user-location)
	- [Get an app color](#get-an-app-color)
	- [Get media](#get-media)
	- [Get current lang](#get-current-lang)
	- [Get space ID](#get-space-id)
	- [Change space ID](#change-space-id)
	- [Navigation](#navigation)
		- [Push new page](#push-new-page)
		- [Close page](#close-page)
		- [Open menu](#open-menu)
		- [Set navigation bar button](#set-navigation-bar-button)
		- [Remove navigation bar button](#remove-navigation-bar-button)
		- [Set plugin controls back press](#set-plugin-controls-back-press)
		- [Set navigation bar child mode](#set-navigation-bar-child-mode)
		- [Go to welcome screen](#go-to-welcome-screen)
		- [Go to notification center](#go-to-notification-center)
	- [Show spinner](#show-spinner)
	- [Camera](#camera)
		- [Take a photo](#take-a-photo)
		- [Read a bar code](#read-a-bar-code)
	- [GPS Tracking](#gps-tracking)
		- [Start tracking](#start-tracking)
		- [Stop tracking](#stop-tracking)
		- [Check if tracking is enabled](#check-if-tracking-is-enabled)
		- [Close App](#close-app)
	- [Wifi features](#wifi-features)
		- [Get Wifi infos](#get-wifi-infos)
		- [Connect to Wifi](#connect-to-wifi)
- [UI](#ui)
	- [Use Fullscreen (iOS only)](#use-fullscreen-ios-only)
		- [Check if Fullscreen enabled](#check-if-fullscreen-enabled)
	- [Set Title](#set-title)
	- [Change status bar appearance](#change-status-bar-appearance)
	- [Change status bar color](#change-status-bar-color)
	- [Get the navigation bar height](#get-the-navigation-bar-height)
	- [Get the status bar height](#get-the-status-bar-height)
- [Platform](#platform)
	- [Get platform version](#get-platform-version)
- [Generic Actions](#generic-actions)
		- [List generic actions available for the plugin](#list-generic-actions-available-for-the-plugin)
	- [Perform a generic action](#perform-a-generic-action)
- [Data storage](#data-storage)
	- [Get items](#get-items)
	- [Get item](#get-item)
	- [Set item](#set-item)
	- [Delete item](#delete-item)
	- [Get files](#get-files)
	- [Get file](#get-file)
	- [Save file](#save-file)
	- [Get Form](#get-form)
- [Manifest](#manifest)
	- [Plugin manifest](#plugin-manifest)
	- [Plugin icon](#plugin-icon)
- [Plugin preview](#plugin-preview)
- [Experimental features](#experimental-features)
	- [iOS - dataConnector](#ios---dataconnector)
- [Custom events](#custom-events)
- [Colors Identifiers](#colors-identifiers)
- [Previous release notes](#previous-release-notes)
	- [New in the 4.00 SDK](#new-in-the-400-sdk)
	- [New in the 2.10 SDK](#new-in-the-210-sdk)
	- [New in the 2.9 SDK](#new-in-the-29-sdk)
	- [New in the 2.8 SDK](#new-in-the-28-sdk)
	- [New in the 2.7 SDK](#new-in-the-27-sdk)
	- [New in the v2.6.0 SDK](#new-in-the-v260-sdk)
	- [New in the v2.4.1 iOS / v 2.4 Android Swizi Player release](#new-in-the-v241-ios--v-24-android-swizi-player-release)
	- [New in the v2.4 iOS / v 2.4 Android Swizi Player release](#new-in-the-v24-ios--v-24-android-swizi-player-release)
	- [New in the v2.3.1 iOS / v 2.3.1 Android Swizi Player release](#new-in-the-v231-ios--v-231-android-swizi-player-release)
	- [New in the v2.3 iOS / v 2.3 Android Swizi Player release](#new-in-the-v23-ios--v-23-android-swizi-player-release)
- [Breaking Change](#breaking-change)
	- [New in the v2.2 iOS / v2.2 Android Swizi Player release](#new-in-the-v22-ios--v22-android-swizi-player-release)
	- [New in the v2.1.2 iOS / v2.1 Android Swizi Player release](#new-in-the-v212-ios--v21-android-swizi-player-release)
	- [New in the v2.0.8 iOS / v2.0.6 Android Swizi Player release](#new-in-the-v208-ios--v206-android-swizi-player-release)
		- [Breaking Changes](#breaking-changes)


# Init
Once the swizi.js script is imported, there's nothing more to do to init the plugin.

You can listen for the event swiziReady. In the event object, in the detail property, you'll find an object called options.

It contains: 

	{
		'showMenuButton': true/false,
		'params': {},
	}

| Param          | Type    | Description                                                                                                                                                                                                                               |
| -------------- | ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| showMenuButton | Boolean | If true, it means the plugin is on top of the navigation pile. So if you're using a side menu navigation, and displaying the plugin in fullscreen, you should display a menu button on top left (call swizi.goBack() to display the menu) |
| params         | Object  | If your plugin is called by a generic action with params, you'll get those params in this object                                                                                                                                          |

# Export your plugin to use it in a Swizi app
Your plugin is ready. You want to use it inside the app (available in the Player too). Don't panic, everything will be fine. 
* 1- Go inside the folder where the files are. You must see the index.html and swizi.js (or swizi.min.js) files here.
* 2- Select all files and folder.
* 3- Then create a zip file with the selection.
* 4- Go to your app in the [Swizi Studio](https://studio.swizi.io)
* 5- Select your plugin (or create it) and upload the zip file created via the big button inside the section.
* 6- Watch it inside the Player.

# Responses
The methods which doesn't return any values don't provide any response.
Other methods (where a result is returned) return a promise. To get the values you need to do like this:

> swizi.getUser().then(function(user) { /* ...your code... */ })

# Logs
You can use this method to log text in the specified window on the phone.

> swizi.log(type, message)

| Param   | Type   | Mandatory |
| ------- | ------ | --------- |
| type    | String | YES       |
| message | String | YES       |

Type can be any value of the console object in the browser.

## Check if plugin bridge is ready
> swizi.isPluginBridgeReady()

Return:

    true or false

# Native calls
This is the differents method you can call and the informations retrieved.

## Login user

> swizi.loginUser(type, login, password, activationCode)

| Param          | Type   | Mandatory                          |
| -------------- | ------ | ---------------------------------- |
| type           | String | YES (*LOGIN* or *ACTIVATION_CODE*) |
| login          | String | YES                                |
| password       | String | YES                                |
| activationCode | String | YES if *type* is *ACTIVATION_CODE* |

Return: JSON Object

		{
			"success": true | false,
			"error": "Error string" 
		}

## Logout user

> swizi.logoutUser()

Return: JSON Object

		{
			"success": true | false,
			"error": "Error string" 
		}

## Get connected user

> swizi.getUser()

Return: JSON Object

    {
      "addressLine1" : null,
      "zipCode" : null,
      "company" : null,
      "country" : null,
      "userID" : "user@user.com",
      "userkey" : null,
      "city" : null,
      "mobilePhone" : null,
      "civility" : 0,
      "fromNetwork" : 0,
      "gender" : 0,
      "phone" : null,
      "login" : "user@user.com",
      "firstName" : "User",
      "role" : null,
      "age" : 0,
      "addressLine2" : null,
      "lastName" : "User",
      "test" : null
    }


## Refresh user datas
Some actions you may have done can change some user datas (application content, user's infos...). If you wish to force the plugin refresh these datas, you can call this method.

> swizi.refreshUserDatas()

Return: no result

## Get current token

> swizi.getToken()

Return: String

## Get AuthWS token

> swizi.getAuthWSToken()

Return: String

## Get user profile photo

> swizi.getUserPhoto(userID)

| Param  | Type   | Mandatory                                                      |
| ------ | ------ | -------------------------------------------------------------- |
| userID | String | NO (if not provided, fetches the current connected user photo) |

Return: see custom events

## Get user location

> swizi.getUserLocation()

Return: see custom events

## Get an app color

> swizi.getColor("COLOR_IDENTIFIER");

| Param            | Type   | Mandatory |
| ---------------- | ------ | --------- |
| Color identifier | String | YES       |

See *colors identifiers* at the end.

Return: JSON Object

    {
      'r': [0 -> 255],
      'g': [0 -> 255],
      'b': [0 -> 255],
      'a': [0 -> 1]
    }

## Get media

When using a manifest inside the plugin, you can add medias to your plugin dynamically. These medias are the one you upload into the library of the Swizi Studio. With this type of configuration field provided, you don't need to include the medias inside the zip file. Bonus point, it will use the same cache system than other medias of your application.

> swizi.getMedia("mediaId")

| Param   | Type   | Mandatory | Description                                             |
| ------- | ------ | --------- | ------------------------------------------------------- |
| mediaID | String | YES       | Id of the media you'll find in the manifest MEDIA value |

Return: JSON Object

    {
      'success' : [0->1],
			'error': null or string,
			'media': base64String,
			'mimeType': mimeTypeString, 
    }

## Get current lang

> swizi.getLang()

Return: JSON Object

	{
		'lang': 'currentLang (fr / en)'
	}

## Get space ID

> swizi.getSpaceId()

Return: JSON Object if there is a space ID

	{
		'spaceId': 'THE_CURRENT_SPACE_ID'
	}

## Change space ID

> swizi.changeSpaceId(id)

| Param | Type   | Mandatory |
| ----- | ------ | --------- |
| id    | String | YES       |

Return: no error if success, otherwise:

	{
		'error': 'reason',
	}

## Navigation
If your plugin use multiple webpage (without a router inside), you can navigate to these pages using native interactions

### Push new page

> swizi.navigateTo("index", null, true);

| Param           | Type          | Mandatory                 |
| --------------- | ------------- | ------------------------- |
| Page name       | String        | YES                       |
| Transition type | String / null | NO - but null if not used |
| Animated        | Bool          | YES                       |

Transition type

* Default: default OS transition
* Modal
* Others will be added in the future 

Return: no result

### Close page

> swizi.back()

Return: no result
NB: if called on the root page and with a side menu navigation, it will open the side menu.

### Open menu

> swizi.openMenu()

Return: no result

### Set navigation bar button
You can add a button in the right of the navigation bar (next to the console button when using Le Player). This button when touched sends a swiziEvent (see [custom events](#custom-events))

> swizi.setNavbarButton("idButton", "absolutePathToIcon")

| Param              | Type   | Mandatory |
| ------------------ | ------ | --------- |
| idButton           | String | YES       |
| absolutePathToIcon | String | YES       |

Return: no result

### Remove navigation bar button

> swizi.removeNavbarButton("idButton")

| Param    | Type   | Mandatory |
| -------- | ------ | --------- |
| idButton | String | YES       |

Return: no result

### Set plugin controls back press
You can take control of the left button of the navigation bar: you can choose if the app stays default, or you can manage yourself the navigation. When calling this method, a "new" button is used instead the default one. When touched, it sends a swiziEvent (see [custom events](#custom-events))

> swizi.setPluginControlsBackPress(takesControl)

| Param         | Type    | Mandatory |
| ------------- | ------- | --------- |
| takesControls | Boolean | YES       |

Return: no result

### Set navigation bar child mode
After taking control of the left button of the navigation bar, you can change its appearance: the burger icon or the back icon. If isChild is set to true, the back icon will be shown. If set to false, the burger icon will be shown.

> swizi.setNavbarChildMode(isChild)

| Param   | Type    | Mandatory |
| ------- | ------- | --------- |
| isChild | Boolean | YES       |

Return: no result

### Go to welcome screen
Sometimes, you may need to go back to the welcome screen (or home) or simple push the welcome screen. You can do that by calling:

> swizi.goToWelcome(popToRoot)

| Param     | Type    | Mandatory                                                                                                                                                               |
| --------- | ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| popToRoot | Boolean | YES - if true, all the navigation to go to the screen you were is lost, and you go back to the welcome screen. if false, the welcome screen is pushed in the navigation |

Return: no result

### Go to notification center
To open the notification center available inside a Swizi application (the application must have the notification center activated into the Notifications menu on the Swizi Studio), you can do that by calling:

> swizi.goToNotificationCenter()

Return: no result

### Open App
You may want to open another app from a plugin. With this method, you can open an app by providing its url-scheme.

> swizi.openUrl(urlScheme, inApp)

Return: JSON object containing a property error which can be null if opening the url is a success.

## Show spinner

> swizi.showSpinner(SHOW_OR_HIDE, MESSAGE);

| Param        | Type   | Mandatory |
| ------------ | ------ | --------- |
| SHOW_OR_HIDE | BOOL   | YES       |
| MESSAGE      | String | NO        |

Return: no result

## Camera

### Take a photo

> swizi.takePicture();

Return: see custom events

### Read a bar code
> swizi.readQRCode();

| List of bar code supported |
| -------------------------- |
| Aztec                      |
| CODABAR                    |
| Code 39                    |
| Code 93                    |
| Code 128                   |
| Data Matrix                |
| EAN-8                      |
| EAN-13                     |
| ITF                        |
| PDF417                     |
| QR Code                    |
| RSS 14                     |
| RSS Expanded               |
| UPCA                       |
| UPCE                       |
| UPC/EAN extension          |

Return: see custom events

## GPS Tracking
You propably use a geolocalizable user group in your app. If so, from a plugin, you can start/stop GPS tracking.

### Start tracking
> swizi.activateGPSTracking()

Return: no result
NB: a local notification is displayed when the GPS tracking is started

### Stop tracking
> swizi.deactivateGPSTracking()

Return: no result
NB: the local notification which display GPS tracking is removed

### Check if tracking is enabled
> swizi.isGPSTrackingEnabled()

Return: JSON Object

    {
      'enabled': true / false 
    }
### Close App
> swizi.closeApp()

Return: None
NB: Quit and close App. Android only feature.

## Wifi features

### Get wifi infos
> swizi.getWifiInfo()

Return: JSON Object
```
    {
        "data": [String],
        "error": String
    }
```

### Connect to Wifi 
> swizi.connectToWifi(ssid, pwd, wifiSecurity, identity, eapType, authType, captivePortal)

(available on iOS and Android 10+)

| Param         | Type   | Mandatory | Description                     |
| ------------- | ----   | --------- | ------------------------------- |
| ssid          | string | YES       | ssid                            |
| pwd           | string | YES       | password                        |
| wifiSecurity  | int    | YES       | wifi security type              |
| identity      | string | YES       | username                        |
| eapType       | int    | YES       | eap type                        |
| authType      | int    | YES       | auth type                       |
| captivePortal | bool   | YES       | if network use a captive portal |

Wifi security values possibility:

| Android  | iOS   | Description                       |
| ---------| ------| --------------------------------- |
| 0        | 0     | Open network                      |
| 1        | 1     | WEP Wi-Fi network                 |
| 2        | 2     | WPA/WPA2 personal Wi-Fi network   |
| 3        | 3     | WPA/WPA2 entreprise Wi-Fi network |

Eap type values possibility:

| Android  | iOS   | Description  |
| ---------| ------| ------------ |
| 0        | 0     | PEAP         |
| 1        | 1     | TLS          |
| 2        | 2     | TTLS         |

Auth type values possibility:

| Android  | iOS   | Description  |
| ---------| ------| ------------ |
| 0        | X     | NONE         |
| X        | 1     | CHAP         |
| 1        | 0     | PAP          |
| 2        | 2     | MSCHAP       |
| 3        | 3     | MSCHAPv2     |
| X        | 4     | EAP          |
| 4        | X     | GTC          |
| 5        | X     | SIM          |
| 6        | X     | AKA          |
| 7        | X     | AKA_PRIME    |

Return: JSON Object

| Android  | iOS   | Description  |
| ---------| ------| ------------ |
| 0        | X     | NONE         |
| X        | 1     | CHAP         |
| 1        | 0     | PAP          |
| 2        | 2     | MSCHAP       |
| 3        | 3     | MSCHAPv2     |
| X        | 4     | EAP          |
| 4        | X     | GTC          |
| 5        | X     | SIM          |
| 6        | X     | AKA          |
| 7        | X     | AKA_PRIME    |

Return: JSON Object

```
    {
        "data": [String],
        "error": String
    }
```
| Description             | Android result                                     | iOS result                                              |
| ----------------------- | -------------------------------------------------- | ------------------------------------------------------- |           
| if wifi has not enabled | data: ["false"],"error": "ERROR wifi not enabled"  | nothing                                                 |
| if device Android 10-   | data: ["false"],"error": "ERROR wifi feature not available on this device"  | nothing                        |
| if error                | data: ["false"],"error": "ERROR when connect to wifi" | data: ["false"],"error": error system description    | 
| OK                      | data: ["true"],"error": "" | data: ["true"],"error": "" |

# UI
To change native components around the web

## Use Fullscreen (iOS only)
You may want to use a fullscreen view for your plugin. 
> swizi.setFullscreen(withAnimation)

| Param         | Type | Mandatory                                   |
| ------------- | ---- | ------------------------------------------- |
| withAnimation | Bool | YES on iOS (doesn't do anything on Android) |

Return: no result
NB: To stop fullscreen view, you can recall this method.

### Check if Fullscreen enabled
> swizi.isFullscreen

## Set Title
To change the title you see in the navigation bar.
> swizi.setTitle('title');

| Param | Type   | Mandatory |
| ----- | ------ | --------- |
| Title | String | YES       |

Return: no result

## Change status bar appearance
> swizi.setStatusBarLight(true);

| Param            | Type | Mandatory |
| ---------------- | ---- | --------- |
| Light appearance | Bool | YES       |

Return: no result

## Change status bar color 
On iOS, it will work only if you set your plugin to use fullscreen. Otherwise, the navigation bar is shown and the status bar is included in it.
> swizi.setStatusColor("#000000");

| Param          | Type        | Mandatory |
| -------------- | ----------- | --------- |
| Color (rrggbb) | hexa string | YES       |

Return: no result

## Get the navigation bar height
> swizi.getNavBarSize();

Return: int

## Get the status bar height
> swizi.getStatusBarSize();

Return: int

# Platform
###Get platform
> swizi.getPlatform();

Return: 

    ios or android

## Get platform version
> swizi.getPlatformVersion()

Return: JSON object

    for iOS:
    {
      "major": 10,
      "minor": 2,
      "patchVersion": 2
    }

| major | minor | patchVersion |
| ----- | ----- | ------------ |
| int   | int   | int          |

    for Android
    {
      "release": "3.4b5",
      "security_patch": "1",
      "sdk_int": "1",
    }

| release | security_patch | sdk_int |
| ------- | -------------- | ------- |
| string  | string         | string  |

# Generic Actions
List and perform generic action you added to your plugin
### List generic actions available for the plugin
> swizi.getGenericActions();

Return: JSON array

	[
		{
			'title': 'GENERIC_ACTION_TITLE',
		}
	]

## Perform a generic action
> swizi.performGenericAction('title', 'params');

| Param  | type   | Description                                                            |
| ------ | ------ | ---------------------------------------------------------------------- |
| title  | string | title of the generic action                                            |
| params | string | key/values parameters to add to the action eg. key1=value1&key2=value2 |

Return: no result

# Data storage
You can store in a private database key/value based informations.
You can also get your API Key Values.
For methods that accept a label parameter, if one is given, it will use the datasource API Key/Values. If none is given, private database key/value based will be used.

Warning: if a user was logged in, databases are cleaned when logout.

## Get items
> swizi.getItems(label);

| Param | Type               | Description             |
| ----- | ------------------ | ----------------------- |
| label | String (optionnal) | Label of the datasource |

Return: JSON array

if label provided

	[
		{
			'label': '...',
			'list': 
					[
						{
							'key': '...',
							'value': '...',
							'scope': '...',
							'param': '...',
						}
					]
		}
	]

if no label provided

	[
		{
			'label': '...',
			'list': 
					[
						{
							'key': '...',
							'value': '...',
							'scope': '...',
							'param': '...',
						}
					]
		}
		...
	]

## Get item
> swizi.getItemForKey(key, label);

| Param | Type              | Description             |
| ----- | ----------------- | ----------------------- |
| key   | String            | Key to access item      |
| label | String (optional) | Label of the datasource |

Return: saved string

## Set item
> swizi.setItemForKey(item, key, label);

| Param | Type              | Description             |
| ----- | ----------------- | ----------------------- |
| item  | String            | Item to store           |
| key   | String            | Key to access item      |
| label | String (optional) | Label of the datasource |

Return: no result

## Delete item
> swizi.removeItemForKey(key);

| Param | Type   | Description        |
| ----- | ------ | ------------------ |
| key   | String | Key to remove item |

Return: no result

## Get files
You can connect a File datasource to a plugin section. With this method, you can get the local URLs of these files inside the application

> swizi.getFiles();

Return: JSON Array

	[
		{
			'folder': 'NAME_OF_THE_DATASOURCE',
			'files': [
				{
					'file': 'LOCAL_URL_OF_THE_FILE',
				},
				...
			]
		},
		...
	]

## Get file
If you have a local URL of a file, you can get the Base64 string of this file.

> swizi.getFile('LOCAL_URL_OF_THE_FILE')

or

> swizi.getFileNamed('THE_NAME_OF_THE_FILE_WITH_ITS_EXTENSION')

Return: JSON Object

	{
		'file': 'base64String',
		'mimeType': 'file's mimeType',
		'error': null,
	}

| Property | Type          | Description                                                              |
| -------- | ------------- | ------------------------------------------------------------------------ |
| file     | Base64 string | The file                                                                 |
| mimeType | String        | The file's MIME-Type                                                     |
| error    | String        | An optional string if an error occured (can be null if everything is ok) |

## Save file
You can save a file in the directory of the plugin.

> swizi.saveFileNamed('BASE_64_REPRESENTATION_OF_THE_FILE', 'FILENAME_WITH_EXTENTION')

Return: JSON Object

	{
		'error': null,
	}

| Property | Type   | Description                                                              |
| -------- | ------ | ------------------------------------------------------------------------ |
| error    | String | An optional string if an error occured (can be null if everything is ok) |

## Get Form
If you have a Form datasource connected to your plugin section, you can access it:

> swizi.getForm()

Return: JSON Object

	{
		'apiKey': 'YOU_API_KEY',
		'name': 'THE_NAME_OF_THE_FORMS',
		'urlMS': 'THE_URL_OF_THE_FORMS',
	}

| Property | Type   |
| -------- | ------ |
| apiKey   | String |
| name     | String |
| urlMS    | String |


# Manifest

Since version 3.x of the SDK, and by using the corresponding Swizi players (September 2019) it is possible to use manifests in plugins.

The manifest allows you to define parameters to configure a plugin from the studio. Plugins can therefore be reused more easily and configured directly from the studio without the need for the plugin developer.

  

> Example: 

 - John creates a weather information plugin. He creates in the plugin a manifest that contains a "city" parameter.
 - Marc creates a mobile application in which he adds a weather section,
   it is a "plugin section". He loads the john's plugin. In the studio,
   he configures the "city" parameter with Paris.
 - Marie, creates another mobile application, she also adds a weather
   section. It sets the "City" parameter to London.
   
Marie and Marc both used the plugin made by John, without knowing how it is made, and configuring it according to their needs.

## Plugin manifest

The manifest is a file named `manifest.json` that must be placed at the root of the plugin package. It is possible to download an example file from the studio, in a plugin section.

In the plugin, to retrieve the settings made in the studio, just call the `getManifest()` method.

The `manifest.json` file accepts the following information: 

| Parameter                 | Required | type     | Usage                                                                                                                              |
| ------------------------- | -------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| pluginManifestVersion     | yes      | string   | manifest format version                                                                                                            |
| name                      | yes      | string   | the name of the plugin as it will be displayed in the studio.                                                                      |
| version                   | yes      | string   | current version of plugin                                                                                                          |
| vendor                    | no       | string   | name of plugin  editor                                                                                                             |
| copyright                 | no       | string   | legal disclaimer                                                                                                                   |
| description               | no       | string   | a short explanation of the plugin purpose                                                                                          |
| help                      | no       | string   | a link url to help for this plugin                                                                                                 |
| whatsNew                  | no       | [string] | a table with a short description of the new features of each version of the plugin                                                 |
| apiKey                    | no       | object   | If this object is set, an APIKey will be created for this app.                                                                     |
| apiKey.scopes             | yes      | [string] | an array of required scope for this apiKey (UserManagement, ContentManagement ...) See apiKey section in Studio to get scopes list |
| apiKey.auth               | yes      | boolean  | specifies if the apiKey must be authentified                                                                                       |
| requirements              | no       | object   | specifies a list of prerequisites for apps that use this plugin                                                                    |
| requirements.privateApp   | no       | boolean  | specifies the app must be private                                                                                                  |
| requirements.mixedApp     | no       | boolean  | specifies the app must be mixed                                                                                                    |
| configuration             | yes      | [object] | list the parameters that must be set in the studio to configure the plugin.                                                        |
| configuration.name        | yes      | string   | parameter name                                                                                                                     |
| configuration.label       | yes      | string   | parameter label                                                                                                                    |
| configuration.description | yes      | string   | parameter short description                                                                                                        |
| configuration.type        | yes      | string   | parameter type. The value will be casted in the getManifest() method. Type can be : STRING, INT, FLOAT, DATA, ENUM, BOUNDS, COLOR  |
| configuration.bounds      | no       | object   | Definition of the bounds of a bounds parameter                                                                                     |
| configuration.bounds.min  | no       | object   | minimum value                                                                                                                      |
| configuration.bounds.max  | no       | object   | maximum value                                                                                                                      |
| configuration.bounds.step | no       | object   | step to use to increment/decrement                                                                                                 |
| configuration.value       | no       |          | default value                                                                                                                      |

## Plugin icon

It is possible to define an icon that represents the plugin. This icon will be used in the studio in the configuration page of the section that uses the plugin.
To define the icon, you must drop a `logo.png` file at the root of the plugin package.

# Plugin preview

It is possible to create a preview version of the plugin. A preview is a javascript/html content that will be displayed inside the tiles of the presentation sections. 
This feature allows you to display dynamic and personalized content in the presentation sections.
The preview of a weather plugin can, for example, be just the temperature and an image representing the weather.
For performance reasons, it is recommended to make content simple, refined with little javascript processing.
To make a preview available, drop a `preview.html` file at the root of the plugin package.

# Experimental features
For more informations, please contact us.

## iOS - dataConnector

> swizi.experimental.ios.dataConnector(dataConnector, datas, withCompletion)

Return: JSON Object if JSON valid, if it has a return value and if withCompletion is set to true

| Property       | Type    | Mandatory | Description                                                                                |
| -------------- | ------- | --------- | ------------------------------------------------------------------------------------------ |
| dataConnector  | Enum    | YES       | Enum available swizi.dataConnector                                                         |
| datas          | Array   | NO        | The datas to pass to the dataConnector                                                     |
| withCompletion | Boolean | NO        | If the call can provide an answer, and if the answer is JSON valid, you can get the result |

# Custom events
Some datas comes from native code. To get result from asynchronous call, you need to give a callback to the next method:

> swizi.onEvent(function(event){});

You can also remove the event listener by calling:

> swizi.removeEvent(function(event){});

To determine which kind of data you'll get, all informations are in *event.detail*:

    {
      'type': 'TYPE_OF_EVENT',
      'value':'VALUE',
      'param': 'PARAM_VALUE',
      'error': 'ERROR_MESSAGE'
    }

| type                 | value         | Description                                                                           | Param                                           | Error                                           |
| -------------------- | ------------- | ------------------------------------------------------------------------------------- | ----------------------------------------------- | ----------------------------------------------- |
| codeReader           | String        | Result of bar code scanning                                                           | None                                            | String with value dismiss if user cancel QRCode |
| userPhoto            | Base64 string | User photo in a Base64 string                                                         | String: userID provided (if none, returns 'me') | String: error message                           |
| location             | Object        | Result of get user location with lat, lon, accuracy and altitude properties           | None                                            | None                                            |
| takePicture          | String        | URL of the photography                                                                | None                                            | String: dismiss if user abort the action        |
| SwiziButtonTouched   | String        | Id of the button touched or onBackPressed                                             | None                                            | None                                            |
| SwiziDataRefreshed   | Object        | An object containing the following informations: token, userId, login, lang, platform | None                                            |
| wifiNetworkAvailable | JSON Object   | Result of wifi network scan                                                           | None                                            | Json Object                                     |

# Colors Identifiers

<table border="0" cellspacing="0" width="1352">
	<tbody>
		<tr height="19">
			<td height="19" width="363"><strong>element_id</strong></td>
			<td width="83"><strong>param_type</strong></td>
			<td width="159"><strong>fk_depends</strong></td>
			<td width="83"><strong>color</strong></td>
			<td width="83"><strong>luminance</strong></td>
			<td width="83"><strong>alpha</strong></td>
			<td width="83"><strong>alpha_hex</strong></td>
			<td width="83"><strong>description</strong></td>
			<td width="83"><strong>theme</strong></td>
			<td width="83"><strong>custom</strong></td>
			<td width="83">&nbsp;</td>
			<td width="83">&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ID_3</td>
			<td>ID</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/333333">#333333</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur fonctionnelle 3</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ID_2</td>
			<td>ID</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur fonctionnelle 2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ID_1</td>
			<td>ID</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/333333">#333333</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur fonctionnelle 1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_USER_COLOR_1</td>
			<td>USER</td>
			<td>&nbsp;</td>
			<td>#FF6600</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur principale</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_USER_COLOR_2</td>
			<td>USER</td>
			<td>&nbsp;</td>
			<td>#FFDC00</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur secondaire 1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_USER_COLOR_3</td>
			<td>USER</td>
			<td>&nbsp;</td>
			<td>#4BB4E6</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur secondaire 2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_HEADER_ICON_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_BACK</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur de fond de chaque élément d'une liste</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TEXT_PRIMARY</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_HEADER_ICON_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de la description de chaque élément d'une liste dans les écrans Liste et Détail</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_TEXT_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de la description de chaque élément d'une liste qui est sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">couleur de fond de chaque élément d'une liste qui est sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_PAGECONTROL_SELECTED_BACK</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond du petit carré&nbsp; sélectionné&quot; sur slideshow</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_BACK_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#EFEFEF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_NAVBAR_BACK</td>
			<td>VAR</td>
			<td>COLOR_ID_3</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="3">Couleur de fond de la barre de navigation</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_TEXT_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_NAVBAR_TEXT</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte de la barre de navigation (titre de l'écran)</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_NAVBAR_ICON_TINT</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur des icônes de la barre de navigation (boutons menu; partager)</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TEXT_TERTIARY</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/333333">#333333</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/474747">#474747</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur de fond des boutons dans les écrans</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond des boutons sélectionnés dans les écrans</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SEPARATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/333333">#333333</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur du séparateur de l'écran d'authentification</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur de fond du menu</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_HEADER_TITLE</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte dans bandeau (des medias) du slideshow</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_HEADER_ICON</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_TITLE_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du titre de chaque élément d'une liste qui est sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_TITLE_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_BACK_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#EFEFEF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond des boutons désactivés dans les écrans</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur du texte des boutons dans les écrans</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_TEXT_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">couleur du texte des boutons sélectionnés dans les écran</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_TEXT_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond des boutons désactivés dans les écrans</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_ICON</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur de l'icône des boutons dans les écrans</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_INTRODUCTION_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_ICON_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de l'icône des boutons sélectionnés dans les écrans</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BUTTON_ICON_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de l'icône des boutons désactivés dans les écrans</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BACK_INTERSTICIAL</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur de fond de l'interstitiel</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BACK_SPLASHSCREEN</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur de fond du splashscreen</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_BACK_WELCOME</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2">Couleur de fond de l'écran d'entrée</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_INTRODUCTION</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/999999">#999999</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur du sous-titre de chaque élément d'une liste dans les écrans Liste et Détail</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_SEPARATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/666666">#666666</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Séparateur dans les listes</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_TITLE</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur du titre de chaque élément d'une liste dans les écrans Liste et Détail</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_PAGECONTROL_SELECTED_BORDER</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur du bord du petit carré&nbsp; sélectionné&quot; sur slideshow</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ACTIVITY_INDICATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FF00BB</td>
			<td>&nbsp;</td>
			<td>0.33</td>
			<td align="right">54</td>
			<td colspan="2">Couleur du spinner</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TEXT_SECONDARY</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/666666">#666666</a></td>
			<td>&nbsp;</td>
			<td>0.75</td>
			<td>BF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_ITEM_INTRODUCTION_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>0.8</td>
			<td>CC</td>
			<td colspan="5">Couleur du sous-titre de chaque élément d'une liste qui est sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_MASK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>0.6</td>
			<td align="right">99</td>
			<td colspan="5">Couleur du masque pour masquer le bout d'écran courant affiché à droite du menu</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_HEADER_ICON_BACK</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>0.5</td>
			<td align="right">80</td>
			<td colspan="4">Couleur de fond du bouton du bandeau du slideshow</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_HEADER_SEPARATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>0.2</td>
			<td align="right">33</td>
			<td>Couleur du séparateur entre partie &quot;titre&quot; et bouton &quot;partager&quot; du bandeau&nbsp; (des medias) du slideshow</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_PAGECONTROL_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>0.6</td>
			<td align="right">99</td>
			<td>Couleur de fond du petit carré &quot;non sélectionné&quot; sur slideshow</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_HEADER_GRADIENT1</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>-0.2</td>
			<td align="right">1</td>
			<td>FF</td>
			<td colspan="5">Couleur du fond du bandeau&nbsp; (des medias) du slideshow avec gradient</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_SLIDESHOW_HEADER_GRADIENT2</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>-0.2</td>
			<td>0.5</td>
			<td align="right">80</td>
			<td colspan="5">Couleur du fond du bandeau&nbsp; (des medias) du slideshow avec gradient</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_BACK_ITEM</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur de fond de chaque rubrique du menu</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond de chaque rubrique du menu quand sélectionnée</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_BACK_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_TEXT</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur du texte de chaque rubrique du menu</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_TEXT_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte de chaque rubrique du menu quand sélectionnée</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_TEXT_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ICON</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur de l'icône de chaque rubrique du menu</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ICON_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de l'icône de chaque rubrique du menu quand sélectionnée</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ICON_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ACCOUNT_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond des items du menu personnel et des boutons en bas du menu</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ACCOUNT_SEPARATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/333333">#333333</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du séparateur entre les items menu personnel</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ACCOUNT_ICON</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur des icônes des items du menu personnel et des boutons en bas du menu</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TAB_LEVEL_1_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur du texte des onglets de niveau 1 (onglets de l'écran &quot;Informations&quot; et onglets &quot;Jour&quot; de l'agenda)</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ACCOUNT_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond des items du menu personnel et des boutons en bas du menu quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ACCOUNT_ICON_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">couleur des icônes des items du menu personnel et des boutons en bas du menu quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ACCOUNT_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte des items du menu personnel et des boutons en bas du menu</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_ACCOUNT_TEXT_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte des items du menu personnel et des boutons en bas du menu quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_LOGOUT_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/474747">#474747</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond du bouton &quot;se déconnecter&quot; du menu personnel</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_LOGOUT_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond du bouton &quot;se déconnecter&quot; du menu personnel quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_LOGOUT_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte du bouton &quot;se déconnecter&quot; du menu personnel</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_LOGOUT_TEXT_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte du bouton &quot;se déconnecter&quot; du menu personnel quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_LOGOUT_ICON</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de l'icône du bouton &quot;se déconnecter&quot; du menu personnel</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_LOGOUT_ICON_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de l'icône du bouton &quot;se déconnecter&quot; du menu personnel quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_BACK</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur de fond des rubriques du quicklaunch</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond des rubriques du quicklaunch quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_BACK_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#EFEFEF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond des rubriques du quicklaunch quand non disponible&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_TEXT</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur du texte des rubriques du quicklaunch</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_TEXT_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte des rubriques du quicklaunch quand non disponible</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_TEXT_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte des rubriques du quicklaunch quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_ICON</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur de l'icône des rubriques du quicklaunch</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_ICON_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de l'icône des rubriques du quicklaunch quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_QL_ICON_DISABLED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de l'icône des rubriques du quicklaunch quand non disponible</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GLOBAL_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de fond de tous les écrans hors écrans de démarrage et dashboard</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_DASHBOARD_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de fond du dashboard</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GALERY_HEADER_TITLE</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte dans bandeau (des medias) de la galerie</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TAB_LEVEL_1_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de sélection des onglets ; qui appara&Atilde;&reg;t sous l'onglet. Concerne les onglets de niveau 1 (onglets de l'écran &quot;Informations&quot; et onglets &quot;Jour&quot; de l'agenda) et de niveau 2 ( (onglets &quot;Site&quot; de l'agenda)</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TAB_LEVEL_2_BACK</td>
			<td>VAR</td>
			<td>COLOR_ID_3</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de fond des onglets de niveau 2 (onglets &quot;Site&quot; de l'agenda)</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GALERY_HEADER_GRADIENT1</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>-0.2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur du fond du bandeau&nbsp; (des medias) de la galerie avec gradient</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TAB_LEVEL_1_SEPARATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/99999">#99999</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du séparateur des onglets de niveau 1 (onglets de l'écran &quot;Informations&quot; et onglets &quot;Jour&quot; de l'agenda) et&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GALERY_HEADER_SEPARATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>0.2</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du séparateur entre partie &quot;titre&quot; et bouton &quot;partager&quot; du bandeau&nbsp; (des medias) de la galerie</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GALERY_HEADER_ICON</td>
			<td>VAR</td>
			<td>COLOR_ID_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GALERY_HEADER_ICON_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TAB_LEVEL_2_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte des onglets de niveau 2 (onglets &quot;Site&quot; de l'agenda)</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TAB_LEVEL_2_SEPARATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/999999">#999999</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du séparateur des onglets de niveau 2 (onglets &quot;Site&quot; de l'agenda)</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GALERY_HEADER_ICON_BACK</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>0.5</td>
			<td align="right">80</td>
			<td colspan="4">Couleur de fond du bouton du bandeau de la galerie</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GALERY_HEADER_ICON_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond du bouton du bandeau de la galerie quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_AGENDA_TITLE</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur du titre d'une session dans les écrans Agenda et Session</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TAB_LEVEL_1_BACK</td>
			<td>VAR</td>
			<td>COLOR_ID_3</td>
			<td>&nbsp;</td>
			<td>0.2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de fond des onglets de niveau 1 (onglets de l'écran &quot;Informations&quot; et onglets &quot;Jour&quot; de l'agenda)</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_AGENDA_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur du texte d'une session dansles écrans Agenda et Session</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_AGENDA_TITLE_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du titre d'une session dans l'Agenda quand sélectionnée</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_AGENDA_ICON</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de l'icône &quot;favori&quot; d'une session dans l'écran Agenda</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_AGENDA_ICON_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de l'icône &quot;favori&quot; d'une session dans l'écran Agenda quand sélectionné</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_AGENDA_SITE</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/999999">#999999</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de l'icône et du nom du site d'une session dans les écrans Agenda et Session</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TIMELINE_TEXT_BEGIN</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#DDDDDD</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de l'heure de début d'une session sur la timeline de l'agenda et couleur de l'heure de diffusion d'une annonce du live</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TIMELINE_TEXT_END</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#CCCCCC</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de l'heure de début d'une session sur la timeline de l'agenda</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TIMELINE_SEPARATOR_1</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/666666">#666666</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>barre verticale (moitié gauche) de la timeline de l'agenda et du live</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TIMELINE_SEPARATOR_2</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>barre verticale (moitié droite de la timeline de l'agenda et du live</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_TIMELINE_BACK</td>
			<td>VAR</td>
			<td>COLOR_ID_3</td>
			<td>&nbsp;</td>
			<td>-0.07</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Couleur de fond de la timeline de l'agenda et du live</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_LIVE_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="4">Couleur du texte des annonces publiques et privées</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_LIVE_PUBLIC</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de la bande verticale à gauche de l'annonce pour une annonce publique</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_LIVE_PUBLIC_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_LIVE_PRIVATE</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_3</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de la bande verticale à gauche de l'annonce pour une annonce publique</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_LIVE_PRIVATE_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#DDDDDD</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_WELCOME_BOTTOM_GRADIENT1</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond de la partie du bas de l'écran d'entrée avec les bouton. 1&Atilde;&uml;re valeur pour le dégradé.</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_WELCOME_BOTTOM_GRADIENT2</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur de fond de la partie du bas de l'écran d'entrée avec les bouton. 2&Atilde;&uml;me valeur pour le dégradé.</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_WELCOME_BOTTOM_TEXT</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du texte de la partie du bas de l'écran d'entrée avec les bouton.</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_WELCOME_BOTTOM_BUTTON_BACK</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_WELCOME_BOTTOM_BUTTON_BACK_SELECTED</td>
			<td>VAR</td>
			<td>COLOR_USER_COLOR_1</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_WELCOME_BOTTOM_BUTTON_ICON</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/000000">#000000</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_WELCOME_BOTTOM_BUTTON_ICON_SELECTED</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td>#FFFFFF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_MENU_BOTTOM_SEPARATOR</td>
			<td>FIXED</td>
			<td>&nbsp;</td>
			<td><a href="/spaces/swizibyopen/tickets/333333">#333333</a></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="5">Couleur du séparateur entre les boutons en bas du menu</td>
		</tr>
		<tr height="19">
			<td height="19">COLOR_GALERY_HEADER_GRADIENT2</td>
			<td>VAR</td>
			<td>COLOR_ID_1</td>
			<td>&nbsp;</td>
			<td>-0.2</td>
			<td>&nbsp;</td>
			<td align="right">0</td>
			<td>Couleur du fond du bandeau&nbsp; (des medias) de la galerie avec gradient</td>
			<td>t</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>


# Previous release notes

## New in the 4.00 SDK

> [Add plugin manifest](#plugin-manifest)

## New in the 2.10 SDK

> Get space ID

> Change space ID

## New in the 2.9 SDK

> Go to welcome screen

> Experimental features

## New in the 2.8 SDK

> Refresh user datas

## New in the 2.7 SDK

> Set navigation bar button

> Remove navigation bar button

> Set plugin controls back press

> Set navigation bar child mode

> Perform generic actions with parameters

## New in the v2.6.0 SDK

> Get current lang

## New in the v2.4.1 iOS / v 2.4 Android Swizi Player release

> Change status bar color

> Get navigation bar height

> Get status bar height

## New in the v2.4 iOS / v 2.4 Android Swizi Player release

Swizi sdk is now available on npm

> npm i --save swizi

## New in the v2.3.1 iOS / v 2.3.1 Android Swizi Player release

> Init update

> Show spinner

> Close page update

## New in the v2.3 iOS / v 2.3 Android Swizi Player release

> Get form

> Save file

> Get File named (see [Get file](#get-file))

# Breaking Change

In the v2.2.1 on iOS and Android, the gamoReady changes its name. It is now swiziReady.

## New in the v2.2 iOS / v2.2 Android Swizi Player release

> Get files

> Get file

## New in the v2.1.2 iOS / v2.1 Android Swizi Player release

> Get user location

> Take a photo 

## New in the v2.0.8 iOS / v2.0.6 Android Swizi Player release

> Get current token

> Get a user profile photo

### Breaking Changes

Gamo has changed its name to Swizi. The plugin is following the same name. Now it is swizi.js and the object is called the same, swizi. **For a few next releases, the gamo object is still availble (you can use one or the other).**

In the previous version, named gamo, the methods returned directly the value. **Now it returns a promise (see [Responses](#responses)).**

Listening for the event gamoReady is not necessary anymore. You can remove your call. **For a few next release, the event will still be fired.**
